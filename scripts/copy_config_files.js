module.exports = function(ctx) {
    var fs = ctx.requireCordovaModule('fs'),
    path = ctx.requireCordovaModule('path'),
    rootdir = ctx.opts.projectRoot,

    android_dir = path.join(ctx.opts.projectRoot, 'platforms', 'android');

//    gradle_file = path.join(android_dir, '/app/src/main/build-extras.gradle');
//    dest_gradle_file = path.join(android_dir, '/app/build-extras.gradle');
//
//    google_services = path.join(android_dir, '/app/src/main/google-services.json');
//    dest_google_services = path.join(android_dir, '/app/google-services.json');

    gradle_properties_file = path.join(android_dir, 'app', 'src', 'main', 'gradle.properties');
    dest_gradle_properties_file = path.join(android_dir, 'gradle.properties');

    console.log("rootdir", rootdir);
    console.log("android_dir", android_dir);
//    console.log("gradle_file", gradle_file);
//    console.log("dest_gradle_file", dest_gradle_file);
//    console.log("google_services", google_services);
//    console.log("dest_google_services", dest_google_services);
    console.log("gradle_properties_file", gradle_properties_file);
    console.log("dest_gradle_properties_file", dest_gradle_properties_file);

    /*if(!fs.existsSync(gradle_file)){
        console.log(gradle_file + ' not found. Skipping');
        return;
    }else if(!fs.existsSync(android_dir)){
        console.log(android_dir + ' not found. Skipping');
       return;
    }else if(!fs.existsSync(google_services)){
       console.log(google_services + ' not found. Skipping');
       return;
    }else*/ if(!fs.existsSync(gradle_properties_file)){
       console.log(gradle_properties_file + ' not found. Skipping');
       return;
    }

//    console.log('Copy  gradle_file  to ' + dest_gradle_file);
//    fs.createReadStream(gradle_file).pipe(fs.createWriteStream(dest_gradle_file));

//    console.log('Copy  google_services  to ' + dest_google_services);
//    fs.createReadStream(google_services).pipe(fs.createWriteStream(dest_google_services));

    console.log('Copy  gradle_properties_file  to ' + dest_gradle_properties_file);
    fs.createReadStream(gradle_properties_file).pipe(fs.createWriteStream(dest_gradle_properties_file));
}
