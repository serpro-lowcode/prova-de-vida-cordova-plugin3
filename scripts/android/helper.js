var fs = require("fs");
var path = require("path");
var xml2js = require('xml2js');
const PLUGIN_ID = "cordova-plugin-prova-de-vida";

function rootBuildGradleExists() {
  var target = path.join("platforms", "android", "build.gradle");
  return fs.existsSync(target);
}

function appBuildGradleExists() {
  var target = path.join("platforms", "android", "app", "build.gradle");
  return fs.existsSync(target);
}

/*
 * Helper function to read the build.gradle that sits at the root of the project
 */
function readRootBuildGradle() {
  var target = path.join("platforms", "android", "build.gradle");
  return fs.readFileSync(target, "utf-8");
}

/*
 * Helper function to read the build.gradle that sits at the app module of the project
 */
function readAppBuildGradle() {
  var target = path.join("platforms", "android", "app", "build.gradle");
  return fs.readFileSync(target, "utf-8");
}

/*
 * Added a dependency on 'com.google.gms' based on the position of the know 'com.android.tools.build' dependency in the build.gradle
 */
function addDependencies(buildGradle) {

  console.log('addDependencies');
  // find the known line to match
  var match = buildGradle.match(/^(\s*)classpath 'com.android.tools.build(.*)/m);

  var whitespace = match[1];

  // modify the line to add the necessary dependencies
  var googlePlayDependency = whitespace + 'classpath \'com.google.gms:google-services:4.2.0\' // google-services dependency from cordova-plugin-prova-de-vida';
//  var kotlinGradlePlugin = whitespace + 'classpath \'org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.50\' // kotlin gradle plugin from cordova-plugin-prova-de-vida'
//  var kotlinAndroidExtentions = whitespace + 'classpath \'org.jetbrains.kotlin:kotlin-android-extensions:1.3.50\' // kotlin gradle plugin from cordova-plugin-prova-de-vida'
  var modifiedLine = match[0] + '\n' + googlePlayDependency;// + '\n' + kotlinGradlePlugin + '\n' + kotlinAndroidExtentions;
  
  // modify the actual line
  return buildGradle.replace(/^(\s*)classpath 'com.android.tools.build(.*)/m, modifiedLine);
}

/*
 * Add 'google()' and Crashlytics to the repository repo list
 */
function addRepos(buildGradle) {
  console.log('addRepos');
  // find the known line to match
  var match = buildGradle.match(/^(\s*)jcenter\(\)/m);
  var whitespace = match[1];

  /*// modify the line to add the necessary repo
  // Crashlytics goes under buildscripts which is the first grouping in the file
  var fabricMavenRepo = whitespace + 'maven { url \'https://maven.fabric.io/public\' } // Fabrics Maven repository from cordova-plugin-prova-de-vida'
  var modifiedLine = match[0] + '\n' + fabricMavenRepo;

  // modify the actual line
  buildGradle = buildGradle.replace(/^(\s*)jcenter\(\)/m, modifiedLine);*/

  var modifiedLine = match[0];

  // update the all projects grouping
  var allProjectsIndex = buildGradle.indexOf('allprojects');
  if (allProjectsIndex > 0) {
    // split the string on allprojects because jcenter is in both groups and we need to modify the 2nd instance
    var firstHalfOfFile = buildGradle.substring(0, allProjectsIndex);
    var secondHalfOfFile = buildGradle.substring(allProjectsIndex);

    // Add google() to the allprojects section of the string
    match = secondHalfOfFile.match(/^(\s*)jcenter\(\)/m);
    var googlesMavenRepo = whitespace + 'google() // Google\'s Maven repository from cordova-plugin-prova-de-vida';
    modifiedLine = match[0] + '\n' + googlesMavenRepo;
    // modify the part of the string that is after 'allprojects'
    secondHalfOfFile = secondHalfOfFile.replace(/^(\s*)jcenter\(\)/m, modifiedLine);

    // recombine the modified line
    buildGradle = firstHalfOfFile + secondHalfOfFile;
  } else {
    // this should not happen, but if it does, we should try to add the dependency to the buildscript
    match = buildGradle.match(/^(\s*)jcenter\(\)/m);
    var googlesMavenRepo = whitespace + 'google() // Google\'s Maven repository from cordova-plugin-prova-de-vida';
    modifiedLine = match[0] + '\n' + googlesMavenRepo;
    // modify the part of the string that is after 'allprojects'
    buildGradle = buildGradle.replace(/^(\s*)jcenter\(\)/m, modifiedLine);
  }

  return buildGradle;
}

/*
 * Applies the google services plugin 'com.google.gms.google-services'
 */
function applyPlugin(buildGradle) {
  return buildGradle + '\n apply plugin: \'com.google.gms.google-services\' // Google services plugin from cordova-plugin-prova-de-vida';
}

function addKotlinSupport(buildGradle) {
    let defaultArgs = {
        kotlin_version: '\text.kotlin_version = \'1.3.50\'\n',
        kotlin_android: 'apply plugin: "kotlin-android"',
        classpath: ' \t\tclasspath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"'
//        apply_plugin = ['kotlin-android-extensions']
    };
    const pluginXML = fs.readFileSync('./plugins/'+PLUGIN_ID+'/plugin.xml').toString();
    const gradle = buildGradle;
    let text = gradle;
    const parser = new xml2js.Parser();
    parser.parseString(pluginXML, (error, config) => {
        if (error) return;
        if (!config.plugin.hasOwnProperty('platform')) return;
        for (let x of config.plugin.platform)
            if (x['$'].name === 'android') {
                if (x['$'].hasOwnProperty('kotlin'))
                if (x.hasOwnProperty('apply-plugin')) ;
                break;
            }

    });

    if (!gradle.match(/ext.kotlin_version/g)) append(defaultArgs.kotlin_version, /buildscript(\s*)\{\s*/g);
    if (!gradle.match(/kotlin-gradle-plugin/g)) append(defaultArgs.classpath, /classpath\s+(['"])[\w.:]+(['"])/g);
//    if (!gradle.match(/apply\s+plugin(\s*:\s*)(['"])kotlin-android(['"])/g)) append(defaultArgs.kotlin_android);
//    if (defaultArgs.apply_plugin)
//        for (let x of defaultArgs.apply_plugin) {
//            const reg = new RegExp(`apply\\s+plugin(\\s*:\\s*)(['"])${x}(['"])`, 'g');
//            if (!gradle.match(reg)) append(`apply plugin: "${x}"`);
//        }

    function append(edit, reg) {
        if (reg === undefined) reg = /com.android.application['"]/g;
        const pos = text.search(reg);
        const len = text.match(reg)[0].length;
        const header = text.substring(0, pos + len);
        const footer = text.substring(pos + len);
        text = header + '\n' + edit + footer;
    }

    return text + '\napply plugin: \'kotlin-android-extensions\'' + '\napply plugin: \'kotlin-android\'';
}

/*
 * Helper function to write to the build.gradle that sits at the root of the project
 */
function writeRootBuildGradle(contents) {
  var target = path.join("platforms", "android", "build.gradle");
  fs.writeFileSync(target, contents);
}

/*
 * Helper function to write to the build.gradle that sits at the app module of the project
 */
function writeAppBuildGradle(contents) {
  var target = path.join("platforms", "android", "app", "build.gradle");
  fs.writeFileSync(target, contents);
}

module.exports = {

  modifyRootBuildGradle: function() {
    // be defensive and don't crash if the file doesn't exist
    if (!rootBuildGradleExists) {
      return;
    }

    console.log("modifyRootBuildGradle");

    var buildGradle = readRootBuildGradle();

    console.log("###########################################\n\n");
    console.log('build.gradle before:')
    console.log(buildGradle)

    // Add Google Play Services Dependency
    buildGradle = addDependencies(buildGradle);
  
    // Add Google's Maven Repo
    buildGradle = addRepos(buildGradle);

    // Add Kotlin Support
//    buildGradle = addKotlinSupport(buildGradle);

    writeRootBuildGradle(buildGradle);
    console.log("###########################################\n\n");
    console.log('build.gradle after:')
    console.log(buildGradle)
  },

  restoreRootBuildGradle: function() {
    // be defensive and don't crash if the file doesn't exist
    if (!rootBuildGradleExists) {
      return;
    }

    console.log("restoreRootBuildGradle");

    var buildGradle = readRootBuildGradle();

    // remove any lines we added
    buildGradle = buildGradle.replace(/(?:^|\r?\n)(.*)cordova-plugin-prova-de-vida*?(?=$|\r?\n)/g, '');
  
    writeRootBuildGradle(buildGradle);
  },

  restoreAppBuildGradle: function() {
      // be defensive and don't crash if the file doesn't exist
      if (!appBuildGradleExists) {
        return;
      }

      console.log("restoreAppBuildGradle");
//      console.log("###########################################\n\n");
//      function getFiles (dir, files_){
//          files_ = files_ || [];
//          var files = fs.readdirSync(dir);
//          for (var i in files){
//              var name = dir + '/' + files[i];
//              if (fs.statSync(name).isDirectory()){
//                  getFiles(name, files_);
//              } else {
//                  files_.push(name);
//              }
//          }
//          return files_;
//      }
//
//      getFiles(path.join("platforms")).forEach(file => {
//        console.log(file);
//      });
//      console.log("###########################################\n\n");

      var buildGradle = readAppBuildGradle();

      // remove any lines we added
      buildGradle = buildGradle.replace(/(?:^|\r?\n)(.*)cordova-plugin-prova-de-vida*?(?=$|\r?\n)/g, '');

      writeAppBuildGradle(buildGradle);
    },

    modifyAppBuildGradle: function() {
        // be defensive and don't crash if the file doesn't exist
        if (!appBuildGradleExists) {
          return;
        }

        console.log("modifyAppBuildGradle");

        var buildGradle = readAppBuildGradle();

//        buildGradle = applyPlugin(buildGradle);

        buildGradle = addKotlinSupport(buildGradle);

        writeAppBuildGradle(buildGradle);
      }
};
