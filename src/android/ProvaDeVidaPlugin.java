/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package br.gov.serpro;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Base64;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.core.app.ActivityCompat;

public class ProvaDeVidaPlugin extends CordovaPlugin {
    public static final int REQUEST_PERMISSION_CAMERA_CARTAO_CODE = 1001;
    private String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public static final int DV_CODE = 999;

    public static final String DV_DATA = "DV_DATA";
    public static final String IS_ALIVE = "IS_ALIVE";

    private CallbackContext callbackContext;

    @Override
    protected void pluginInitialize() {
        final Context context = this.cordova.getActivity().getApplicationContext();
        this.cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                Log.d("ProvaDeVidaPlugin", "Starting Firebase plugin");
//                FirebaseApp.initializeApp(context);
            }
        });
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        if ("provaDeVida".equals(action)) {
            Activity activity = this.cordova.getActivity();
            if (!possuiPermissoes(activity)) {
                ActivityCompat.requestPermissions(activity,
                    PERMISSIONS, REQUEST_PERMISSION_CAMERA_CARTAO_CODE);
            } else {
                this.callbackContext = callbackContext;
                cordova.setActivityResultCallback(this);
                activity.startActivityForResult(new Intent(cordova.getActivity(), br.gov.serpro.FaceTrackerActivity.class), DV_CODE);
            }
        } else {
            callbackContext.error("Ação chamada inesperada para o plugin. Disponíveis: provaDeVida");
            return false;
        }

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (intent != null) {
            if (requestCode == DV_CODE) {
                if (resultCode != Activity.RESULT_OK) {
                    callbackContext.error("Resultado com erro");
                }
                // Imagem resultado da Detecção de Vida acessível via ByteArrayExtra
                byte[] bytesFotoDaFace = intent.getByteArrayExtra(DV_DATA);
                if (bytesFotoDaFace != null) {
                    JSONObject r = new JSONObject();
                    try {
                        r.put("isAlive", intent.getBooleanExtra(IS_ALIVE, false));
                        r.put("foto", Base64.encodeToString(bytesFotoDaFace, Base64.NO_WRAP));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    callbackContext.success(r);
                } else {
                    callbackContext.error( "Selfie não tirada na prova de vida");
                }
            } else {
                callbackContext.error("Chamada inesperada: " + requestCode);
            }
        } else {
            callbackContext.error("Câmera fechada");
        }
    }

    private static boolean possuiPermissoes(Context ctx) {
        return (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

}
